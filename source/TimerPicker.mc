//!
//! Copyright 2020 by HarryOnline
//!
//! Timer picker
//!

using Toybox.Graphics as Gfx;
using Toybox.WatchUi as Ui;
using Toybox.Application as App;

class TimerPicker extends Ui.Picker {

    function initialize() {
        var title = new Ui.Text({:text=>Rez.Strings.SetTime, :locX =>Ui.LAYOUT_HALIGN_CENTER, :locY=>Ui.LAYOUT_VALIGN_BOTTOM, :color=>Gfx.COLOR_WHITE});
        var factory = new TimeFactory( 100, 5, {:font=>Gfx.FONT_MEDIUM});
        var current = factory.getIndex(App.getApp().getProperty("intervalTime"));
        Picker.initialize({:title=>title, :pattern=>[factory], :defaults => [current]});
    }

    function onUpdate(dc) {
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_BLACK);
        dc.clear();
        Picker.onUpdate(dc);
    }
}

class TimerPickerDelegate extends Ui.PickerDelegate {

    function initialize() {
        PickerDelegate.initialize();
    }

    function onCancel() {
      widgetTimer.reset();
        Ui.popView(Ui.SLIDE_LEFT);
        return true;
    }

    function onAccept(values) {
      widgetTimer.reset();
      App.getApp().setProperty("intervalTime", values[0]);
        Ui.popView(Ui.SLIDE_LEFT);
        Ui.popView(Ui.SLIDE_LEFT);
        return true;
    }
}

class TimeFactory extends Ui.PickerFactory {
    var mMax;
    var mStep;
    var mFont;

    function initialize(max, step, options) {
        PickerFactory.initialize();
        mMax = max;
        mStep = step;
        if( mStep < 1 ) {
          mStep = 1;
        }

        if(options != null) {
            mFont = options.get(:font);
        }

        if(mFont == null) {
            mFont = Gfx.FONT_LARGE;
        }
    }

    function getIndex(value) {
      var result = (value / mStep).toNumber() - 1;
      return result;
    }

    function getSize() {
        return (mMax / mStep).toNumber();
    }

    function getValue(index) {
        return mStep * (index + 1 );
    }

    function getDrawable(index, selected) {
        return new Ui.Text({:text=>getValue(index).toString(), :color=>Gfx.COLOR_WHITE, :font=>mFont, :locX=>Ui.LAYOUT_HALIGN_CENTER, :locY=>Ui.LAYOUT_VALIGN_CENTER});
    }
}
