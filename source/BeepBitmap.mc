//!
//! Copyright 2020 by HarryOnline
//!
//! Beep Bitmap
//!

using Toybox.WatchUi as Ui;
using Toybox.Application as App;

class BeepBitmap extends Ui.Drawable {

  hidden var imgBeep;
  hidden var imgSilent;
  hidden var mX;
  hidden var mY;

  function initialize(params) {
    Drawable.initialize(params);
    mX = params.get(:x);
    mY = params.get(:y);
    imgBeep = Ui.loadResource( Rez.Drawables.Beep);
    imgSilent = Ui.loadResource( Rez.Drawables.Silent);
  }

  function draw(dc) {
      var bell = App.getApp().getProperty("beep") && Attention has :playTone ? imgBeep : imgSilent;
      dc.drawBitmap( mX, mY, bell);
  }
}