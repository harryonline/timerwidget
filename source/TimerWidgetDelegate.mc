//!
//! Copyright 2020 by HarryOnline
//!
//! Delegate
//!

using Toybox.WatchUi as Ui;
using Toybox.Timer;
using Toybox.System;
using Toybox.Application as App;
using Toybox.Attention;

class TimerWidgetDelegate extends Ui.BehaviorDelegate {

  function initialize() {
    BehaviorDelegate.initialize();
    if( App.getApp().getProperty("beep") == null ) {
      App.getApp().setProperty("beep", Attention has :playTone);
    }
  }

  function onSelect() {
    widgetTimer.reset();
    timer.reset();
    return true;
  }

  function onKey(evt) {
    widgetTimer.reset();
    return false;
  }

  function onMenu() {
    widgetTimer.reset();
    Ui.pushView(new Rez.Menus.TimerMenu(), new TimerMenuDelegate(), Ui.SLIDE_RIGHT);
    return true;
  }
}

class TimerMenuDelegate extends Ui.MenuInputDelegate {

  function initialize() {
      MenuInputDelegate.initialize();
  }

  function onMenuItem(item) {
    widgetTimer.reset();
    if (item == :timer_beep && Attention has :playTone) {
      var app = App.getApp();
      var newBeep = !app.getProperty("beep");
      app.setProperty("beep", newBeep );
      if ( Attention has :playTone) {
        Attention.playTone(newBeep ? Attention.TONE_START : Attention.TONE_STOP);
      }
    } else if (item == :timer_time) {
      Ui.pushView(new TimerPicker(), new TimerPickerDelegate(), Ui.SLIDE_RIGHT);
    } else if (item == :about) {
      Ui.pushView(new AboutView(), new AboutDelegate(), Ui.SLIDE_RIGHT );
    }
  }
}

