//!
//! Copyright 2020 by HarryOnline
//!
//! Progressbar on round watches
//!

using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class ProgressRound extends Ui.Drawable {
  function initialize(params) { Drawable.initialize(params); }

  function draw(dc) {
    var fraction = timer.fraction();
    var degrees = fraction * 360;
    var w = 6;
    var x = dc.getWidth() / 2;
    var y = dc.getHeight() / 2;
    var r = x - w / 2;
    dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
    if (degrees == 0) {
      dc.setPenWidth(1);
      dc.drawLine(x, 0, x, w);
    } else {
      dc.setPenWidth(w);
      dc.drawArc(x, y, r, Gfx.ARC_CLOCKWISE, 90, 90 - degrees);
    }
  }
}

class ProgressOctagon extends Ui.Drawable {
  function initialize(params) { Drawable.initialize(params); }

  function draw(dc) {
    var fraction = timer.fraction();
    var degrees = fraction * 360;
    var w = 12;
    var x = dc.getWidth() / 2;
    var y = dc.getHeight() / 2;
    var r = y - w / 2;
    dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
    if (degrees == 0) {
      dc.setPenWidth(1);
      dc.drawLine(x, 0, x, w);
    } else {
      dc.setPenWidth(w);
      dc.drawArc(x, y, r, Gfx.ARC_CLOCKWISE, 90, 90 - degrees);
    }
  }
}

class ProgressSemiRound extends ProgressRound {
  function initialize(params) { ProgressRound.initialize(params); }

  function min(a, b) { return a < b ? a : b; }

  function draw(dc) {
    ProgressRound.draw(dc);
    var fraction = timer.fraction();
    var x = dc.getWidth() / 2;
    var w = 12;
    dc.setPenWidth(w);
    var multiplier = 600;
    var y1 = w / 2;

    dc.drawLine(x, y1, x + min(fraction, 0.1) * multiplier, y1);
    if (fraction <= 0.4) {
      return;
    }

    var y2 = dc.getHeight() - w / 2;
    dc.drawLine(x + 0.1 * multiplier, y2,
                x - min(fraction - 0.5, 0.1) * multiplier, y2);

    if (fraction > 0.9) {
      dc.drawLine(x - 0.1 * multiplier, y1, x + (fraction - 1) * multiplier,
                  y1);
    }
  }
}