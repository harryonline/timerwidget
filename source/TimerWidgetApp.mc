//!
//! Copyright 2020 by HarryOnline
//!
//! Main App
//!

using Toybox.Application as App;
using Toybox.System;

var appVersion = "1.3.21";
var timer;

var widgetTimer;

class TimerWidgetApp extends App.AppBase {
  function initialize() {
    AppBase.initialize();
    App.getApp().setProperty("appVersion", appVersion);
  }

  //! onStop() is called when your application is exiting
  function onStop(state) {
    if( widgetTimer != null && widgetTimer.flag && Attention has :playTone) {
      Attention.playTone(Attention.TONE_FAILURE);
    }
  }

  //! Return the initial view of your application here
  function getInitialView() {
  widgetTimer = new WidgetTimer();
    return [ new TimerWidgetView(), new TimerWidgetDelegate() ];
  }

  function getGlanceView() {
    return [ new GlanceView()];
  }
}