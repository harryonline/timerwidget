//!
//! Copyright 2020 by HarryOnline
//!
//! Progressbar on Widget Timer
//!

using Toybox.Timer;
using Toybox.System;
using Toybox.WatchUi as Ui;

class WidgetTimer {

  var flag = false;
  var timer;
  var delay = 55;

  function initialize() {
    timer = new Timer.Timer();
  }

  function reset() {
    flag = false;
    timer.stop();
    start();
  }

  function start() {
    timer.start( method(:expire), delay*1000, false);
  }

  function expire() {
    flag = true;
  }
}
