//!
//! Copyright 2020 by HarryOnline
//!
//! Timer view
//!

using Toybox.WatchUi;

class TimerWidgetView extends WatchUi.View {

  function initialize() {
    View.initialize();
    timer = new TimerWidget();
    widgetTimer.start();
  }

  //! Load your resources here
  function onLayout(dc) {
    setLayout(Rez.Layouts.MainLayout(dc));
  }

  // Update the view
  function onUpdate(dc) {
    // Call the parent onUpdate function to redraw the layout
    View.findDrawableById("time_remaining").setText( timer.tCount.toString());
    View.findDrawableById("num_count").setText( timer.numCount.toString());
    View.findDrawableById("clock_time").setText( timer.getClock());
    View.onUpdate(dc);
  }
}
