//!
//! Copyright 2020 by HarryOnline
//!
//! Progressbar on square watches
//!

using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class ProgressSquare extends Ui.Drawable {

    function initialize(params) {
        Drawable.initialize(params);
    }

	function draw(dc) {
		var fraction = timer.fraction();
		var length = 1 + fraction * dc.getWidth();
        var w = 12;
    	dc.setPenWidth(w);
        dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
		dc.drawLine(0, w/2, length, w/2);   
	} 
}