//!
//! Copyright 2020 by HarryOnline
//!
//! Timer widget
//!

using Toybox.Timer;
using Toybox.System;
using Toybox.Application as App;
using Toybox.Attention;
using Toybox.WatchUi as Ui;

class TimerWidget {
  var tCount;
  var numCount;
  var updater;
  var tInterval;
  var is24Hour;

  function initialize() {
    tInterval = App.getApp().getProperty("intervalTime").toNumber();
    tCount = tInterval;
    numCount = 0;
    updater = new Timer.Timer();
    updater.start(method(:update), 1000, true);
    var mySettings = System.getDeviceSettings();
        is24Hour = mySettings.is24Hour;
  }

  function reset() {
    updater.stop();
    updater.start(method(:update), 1000, true);
    tInterval = App.getApp().getProperty("intervalTime").toNumber();
    tCount = tInterval;
    Ui.requestUpdate();
  }

  function restart() {
    tInterval = App.getApp().getProperty("intervalTime").toNumber();
    tCount += tInterval;
    numCount ++;
    Ui.requestUpdate();
  }

  function getClock() {
    var clockTime = System.getClockTime();
    var hour = clockTime.hour;
    if( !is24Hour ) {
      hour = (hour + 11) % 12 + 1;
    }
    var timeString = Lang.format("$1$:$2$", [hour, clockTime.min.format("%02d")]);
    return timeString;
  }

  function fraction() {
    return tCount == 0 ? 0 : 1.0 * (tInterval - tCount ) / tInterval;
  }

  function update() {
    tCount --;

    if( tCount < 0  ) {
      restart();
    } else if (tCount == 0 ) {
      vibrate();
    } else if ( tCount <= 5 ) {
      beep();
    }
    Ui.requestUpdate();
  }

  function vibrate() {
    if (App.getApp().getProperty("beep") && (Attention has :playTone)) {
      Attention.playTone(Attention.TONE_LAP);
    }
    if (Attention has :vibrate) {
      var vibrateData = [
        new Attention.VibeProfile(  25, 100 ),
        new Attention.VibeProfile(  50, 100 ),
        new Attention.VibeProfile(  75, 100 ),
        new Attention.VibeProfile( 100, 100 ),
        new Attention.VibeProfile(  75, 100 ),
        new Attention.VibeProfile(  50, 100 ),
        new Attention.VibeProfile(  25, 100 )
      ];
      Attention.vibrate( vibrateData );
    }
  }

  function beep() {
    if (App.getApp().getProperty("beep") && (Attention has :playTone)) {
      Attention.playTone(Attention.TONE_LOUD_BEEP);
    } else if (Attention has :vibrate) {
      var vibrateData = [ new Attention.VibeProfile( 100, 50 )];
      Attention.vibrate( vibrateData);
    }
  }

}