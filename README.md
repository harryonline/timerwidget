A repeating timer widget for Connect IQ compatible Garmin devices, e.g. for interval exercises or stretching.

As soon as you open the widget, it starts counting from the set time to zero. This is repeated until the widget stops. During any count down, you can press Start/Stop to start from the set time again.

As this is a widget, it is very quick to access and can run while you have an App running. However, a widget returns to the watch screen after around two minutes if you don't interact with it, so you have to press Start/Stop every now and then.

Using the Menu button, in the Connect App or in Garmin Express, you can set the time. With the Menu button, you can also disable the beeps. If disabled, the device will only vibrate when it reaches 0.

Next to the remaining seconds, it shows the number of repetitions and the clock time. If you need more features, better use one of the many Apps.

For interval exercises, set the time to the exercise time, start exercising until zero. Take some rest, the timer tells how long it was,  press Start/Stop again when you start the next exercise. 